## small path 1
* greet OR help
    - utter_greet
  
## small path 2
* stop OR swear
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart

## small path 3
* bye
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart
    
## happy path
* greet OR help
    - utter_greet
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* affirm
    - utter_glad_to_hear
    - utter_is_there_something_else
* deny OR stop OR swear
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart
    
## happy path 2
* greet OR help
    - utter_greet
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* affirm
    - utter_glad_to_hear
    - utter_is_there_something_else
* affirm OR help
    - utter_describe_next
    
## happy path 3
* greet OR help
    - utter_greet
* affirm 
    - utter_ask_for_description
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* affirm
    - utter_glad_to_hear
    - utter_is_there_something_else
    
## another story
    - utter_is_there_something_else
* affirm OR help
    - utter_describe_next
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help

## unhappy path
* greet OR help
    - utter_greet
* affirm 
    - utter_ask_for_description
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* deny OR stop OR swear
    - action_contact_technical_help
    - utter_is_there_something_else
* deny OR stop OR swear
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart
    
## another utter story
    - utter_did_it_help
* deny OR stop OR swear
    - action_contact_technical_help
    - utter_is_there_something_else
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* deny OR stop OR swear
    - action_contact_technical_help
    - utter_is_there_something_else
* deny OR stop OR swear
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart

## another utter story
    - utter_did_it_help
* deny OR stop OR swear
    - action_contact_technical_help
    - utter_is_there_something_else
* affirm OR help
    - utter_describe_next
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* affirm
    - utter_glad_to_hear
    - utter_is_there_something_else
* deny OR stop OR swear
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart

## utter describe next branch
    - utter_describe_next
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* affirm
    - utter_glad_to_hear
    - utter_is_there_something_else
* deny OR stop OR swear
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart
    
## unhappy path
* greet OR help
    - utter_greet
* cleanwintemp OR cleanlocaltemp OR Spooler-Queue-Reset OR vpnissue OR slowcomputer OR PrintServer-Reload OR pcvirus OR passwordreset OR mappeddrive OR filerecovery OR emptyrecyclebin OR DHCP-NIC-Refresh OR StartCleanup OR IpConfigRenew OR LowDiskSpace OR DisableStartUps OR ClearCacheCookies OR ResolvePrinterIssues OR DiskCleanup OR UpdateDisplayDriver OR WinUpdates OR MapNetworkDriveandVPN OR StopNotRespondingApps OR ScanHardware
    - action_run_script
    - utter_did_it_help
* deny OR stop OR swear
    - action_contact_technical_help
    - utter_is_there_something_else
* deny OR stop OR swear
    - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
    - action_save_satisfaction
    - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
    - utter_goodbye
    - action_restart

## out of scope path
* contact_human
   - action_contact_technical_help
   - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
   - action_save_satisfaction
   - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
   - utter_goodbye
   - action_restart
   
## continue story
* continue
    - action_listen
   
## satisfaction story
* neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
   - action_save_satisfaction
   - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
   - utter_goodbye
   - action_restart

## restart story
* conversation_restart
   - action_survey
* deny OR stop OR swear OR bye OR neutral OR dissatisfied OR satisfied OR very_dissatisfied OR very_satisfied
   - action_save_satisfaction
   - action_retrieve_logs
* log_message{"local_logs": "some local logs"}
   - utter_goodbye
   - action_restart

## rephrase
* trigger_rephrase
  - utter_rephrase
  - action_listen

## continue
* continue
  - action_listen