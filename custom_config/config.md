## here I am keeping the names of the scripts
script_names:
  - cleanwintemp
  - cleanlocaltemp
  - 'Spooler-Queue-Reset'
  - vpnissue
  - slowcomputer
  - 'PrintServer-Reload'
  - pcvirus
  - passwordreset
  - mappeddrive
  - filerecovery
  - emptyrecyclebin
  - 'DHCP-NIC-Refresh'
  - StartCleanup
  - IpConfigRenew
  - LowDiskSpace
  - DisableStartUps
  - ClearCacheCookies
  - ResolvePrinterIssues
  - DiskCleanup
  - UpdateDisplayDriver
  - WinUpdates
  - MapNetworkDriveandVPN
  - StopNotRespondingApps
  - ScanHardware 
  
## here I am keeping the execution actions
execution_actions:
  "buttons": "<BUTTONS>"
  "survey": "<SURVEY>"
  "script": "<SCRIPT>"