#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on 2020 October 03 20:40:03 (EST) 

@author: KanExtension
"""

import boto3
from botocore.exceptions import ClientError
import pandas as pd
import pandas.io.sql as sqlio
import psycopg2
import datetime
import json
import yaml
from pandas.io.json import json_normalize

'''
M A I N   F U N C T I O N S
'''


def send_mail(sender: str, recepient: str, subject: str, body_text: str, aws_region: str = 'us-east-1'):
    """This function sends an email using the AWS service.

    Parameters
    ----------
    sender: str
        this email must represent the AWS verified email address
    recepient: str
        is the recepient email
    subject
    body_text: str
    aws_region

    Returns
    -------

    Notes
    -----
    - note, this function was written according to this post:
        -
    - note, your docker (or whatever environment) must have the correct environment variables; this means that
    you must set up in shell:
        AWS_ACCESS_KEY_ID=<YOUR ACCESS KEY>
        AWS_SECRET_ACCESS_KEY=<YOUR SECRET ID>

    - note, all the email addresses must be verified. The verification can be done via:
        https://console.aws.amazon.com/ses/home?region=us-east-1#verified-senders-email:

    - note, in practice we can also remove the verification step (in production) and here is the process
    how to do that:
        https://docs.aws.amazon.com/ses/latest/DeveloperGuide/request-production-access.html

    - note, those keys (you are having in S_P_E_M) or if you do not have it, then you must create a new profile

    """
    # -- step 0 -- define the body of the html
    _body_html = \
        f"""<html>
        <head></head>
        <body>
          <h1>The BAIIR problem report.</h1>
          <p>This is the body: {body_text}. You can also add links:
            <a href='http://example.com/'>Example domain</a> 
        </a>.</p>
        </body>
        </html>
        """

    # -- step 1 -- define the charset
    _charset = "UTF-8"

    # -- step 2 -- create the new SES resource
    _client = boto3.client('ses', region_name=aws_region)

    # -- step 3 -- try to send an email
    # Try to send the email.
    try:
        # Provide the contents of the email.
        response = _client.send_email(
            Destination={
                'ToAddresses': [
                    recepient,
                ],
            },
            Message={
                'Body': {
                    'Html': {
                        'Charset': _charset,
                        'Data': _body_html,
                    },
                    'Text': {
                        'Charset': _charset,
                        'Data': body_text,
                    },
                },
                'Subject': {
                    'Charset': _charset,
                    'Data': subject,
                },
            },
            Source=sender,
            # If you are not using a configuration set, comment or delete the
            # following line
            #ConfigurationSetName=CONFIGURATION_SET,
        )
    # Display an error if something goes wrong.
    except ClientError as e:
        # TODO: here you want to use the loging to log the error message
        print(e.response['Error']['Message'])
    else:
        print("Email sent! Message ID:"),
        print(response['MessageId'])



def get_logs(sender_id: str):
    """This function sends gets the log for the particular sender_id.

    Parameters
    ----------
    sender_id: str
        the sender_id in rasa tracker store for the user

    Returns
    -------
    log_string: str
        the latest log string from the db

    Notes
    -----
    - this function first calls the db_connect() function to connect to the database
    - needs to be updated based on exactly what information is needed

    """
    connection = db_connect()
    read_query = "SELECT * FROM public.events WHERE sender_id = '{}' and intent_name = 'log_message' ORDER BY id DESC LIMIT 3;".format(sender_id)
    a = sqlio.read_sql_query(read_query, connection)
    a['timestamp'] = pd.to_datetime(a['timestamp'],unit='s')
    a.reset_index(inplace=True)
    a_df = json_normalize(a['data'].apply(json.loads).tolist()).reset_index()
    a = pd.merge(a,a_df, on='index')

    #closing db connection
    connection.close()

    return a['parse_data.text'][0]

def db_connect():
    """This function creates the connection to the database

    Parameters
    ----------

    Returns
    -------
    
    Notes
    -----

    """
    
    with open('endpoints.yml') as file:
        db_config = yaml.load(file, Loader=yaml.FullLoader)

    #connection to db
    connection = psycopg2.connect(user = db_config['tracker_store']['username'],
                                    password = db_config['tracker_store']['password'],
                                    host = db_config['tracker_store']['url'],
                                    port = "5432",
                                    database = db_config['tracker_store']['db'])

    # cursor = connection.cursor()
    # # Print PostgreSQL version
    # cursor.execute("SELECT version();")
    # record = cursor.fetchone()
    # print("Connected to - ", record,"\n")
    return connection


# testing the functions
# logs = get_logs("DESKTOP-GQJODG9-Jeet")
# print(logs)