# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.events import (SlotSet, UserUtteranceReverted, ConversationPaused, EventType,
                             ActionExecuted, UserUttered, Restarted)
from rasa_sdk.events import FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import yaml
import random
import os
import pandas as pd
import logging
import json
import email_sender as es

# import the custom config file
with open(os.path.join(os.path.split(__file__)[0], 'custom_config', 'config.md'), 'r') as handle:
    _config = yaml.safe_load(handle)

# create the logger
logger = logging.getLogger(__name__)

'''
GLOBALS
'''

# here you are keeping the counts for number of repeats of reformulate policy
_COUNT = 0

# this means, if the number of repeats is more than 3 then it will default to restart
_UPPER_BOUND = 3


class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_run_script"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        """

        Parameters
        ----------
        dispatcher
        tracker
        domain

        Returns
        -------

        Notes
        -----
        - note, all past events can be obtained via: tracker.events
        - note, the current nlu state can be obtained via: tracker.latest_message
        """
        _THRESHOLD = 0.7

        # -- step 0 -- grab intent rankings
        _scripts_and_probabilities = tracker.latest_message['intent_ranking']

        # -- clean it, so they contain only the probabilities coming from script intents
        _scripts_and_probabilities = map(lambda x: tuple(x.values()), _scripts_and_probabilities)
        _scripts_and_probabilities = filter(lambda x: x[0] in set(_config["script_names"]), _scripts_and_probabilities)

        _scripts_and_probabilities = {'scrips_and_probabilities': sorted(_scripts_and_probabilities,
                                                                         key=lambda x: x[-1], reverse=True)}

        # -- step 1 -- you must reformulate if the first of sorted is too small or
        # this is the case, when the NLU decides that the intent is to solve concrete issue, but we do not have
        # enough info
        if _scripts_and_probabilities['scrips_and_probabilities'] and \
                _scripts_and_probabilities['scrips_and_probabilities'][0][-1] < _THRESHOLD:

            return [FollowupAction(name="action_default_ask_affirmation")]

        elif not _scripts_and_probabilities['scrips_and_probabilities']:
            # this branch is when you do not find anything

            ## THIS IS BAD
            return [FollowupAction(name="action_default_ask_affirmation")]

        else:

            dispatcher.utter_message(text=f"Running script: "
                                          f"{_scripts_and_probabilities['scrips_and_probabilities'][0]}")

            dispatcher.utter_custom_json(json_message={'scripts_and_probabilities':
                                                           _scripts_and_probabilities['scrips_and_probabilities'][0]})

            #dispatcher.utter_message(text=f"<Running scripts>", json_message=_scripts_and_probabilities)
            return []


class ActionContactTechnicalHelp(Action):
    """This is the custom action that will contact the technical help

    """
    def name(self):
        return "action_contact_technical_help"

    def run(self, dispatcher, tracker, domain):
        """

        Parameters
        ----------
        dispatcher
        tracker
        domain

        Returns
        -------

        """
        # -- step 0 -- send the confirmation that you are contacting the human help
        dispatcher.utter_message(template='utter_contact_human_help')

        # -- step 1 -- send an actual email (using this service the email must be verified.)
        _result = es.send_mail(sender='kapustnica2008@gmail.com',
                               recepient='matho.polacek@gmail.com',
                               subject='BAIIR ERROR REPORT',
                               body_text='<EXAMPLE ERROR REPORT>')
        return []


class ActionSurvey(Action):
    """This is the action that will spin up the survey

    """
    def name(self):
        return "action_survey"

    def run(self, dispatcher, tracker, domain):
        """

        Parameters
        ----------
        dispatcher
        tracker
        domain

        Returns
        -------

        """
        # -- globals --
        _preparation_to_finish = ['It looks like you want to finish the conversation.',
                                  'It seems you want to finish the conversation.']

        _is_ok_but_survey = ["It's ok, but before you go could you help us to improve our service.",
                             "Before you go, let us ask one more question."]

        _survey_ask = ['How satisfied you were with the conversation?',
                       'Was the conversation helpful?',
                       'Could you rate the the conversation?']

        # -- step 0 -- create the survey buttons
        _buttons = [{'title': 'Very satisfied.', 'payload': '/very_satisfied{}'},
                    {'title': 'Satisfied.', 'payload': '/satisfied{}'},
                    {'title': 'Neutral.', 'payload': '/neutral{}'},
                    {'title': 'Dissatisfied.', 'payload': '/dissatisfied{}'},
                    {'title': 'Very dissatisfied.', 'payload': '/very_dissatisfied{}'}]

        # -- step 1 -- get the message of the survey
        _message_title = random.choice(_survey_ask)

        # -- step 2 -- prepare to finish
        dispatcher.utter_message(text=random.choice(_preparation_to_finish))

        dispatcher.utter_message(text=random.choice(_is_ok_but_survey))

        # -- step 3 -- send the survey
        dispatcher.utter_message(text=_message_title, buttons=_buttons)

        return []


class ActionSaveSatisfaction(Action):
    """

    """
    def name(self):
        return "action_save_satisfaction"

    def run(self, dispatcher, tracker, domain):
        """This function saves the satisfaction score for given user into some database (in AWS)

        Parameters
        ----------
        dispatcher
        tracker
        domain

        Returns
        -------

        """
        print('<SAVE SATISFACTION HERE>')
        return []


class ActionRestarted(Action):
    """ This is for restarting the chat"""

    def name(self):
        return "action_chat_restart"

    def run(self, dispatcher, tracker, domain):
        return [Restarted()]


# two stage fallback policy
class ActionDefaultFallback(Action):
    """This action defines the default fallback.

    """
    def name(self) -> Text:
        return "action_default_fallback"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[EventType]:
        """

        Parameters
        ----------
        dispatcher
        tracker
        domain

        Returns
        -------

        """
        # Fallback caused by TwoStageFallbackPolicy
        if len(tracker.events) >= 4 and tracker.events[-4].get("name") == "action_default_ask_affirmation":
            dispatcher.utter_message(template="utter_restart_with_button")
            # this is original
            #return [SlotSet("feedback_value", "negative"), ConversationPaused()]

            # this is a new one
            #return [ConversationPaused()]

        # Fallback caused by Core
        else:
            #dispatcher.utter_message(template="utter_default")
            dispatcher.utter_message(template="utter_default")

        return [UserUtteranceReverted()]


class ActionDefaultAskAffirmation(Action):
    """Asks for an affirmation of the intent if NLU threshold is not met."""

    def name(self) -> Text:
        return "action_default_ask_affirmation"

    def __init__(self) -> None:

        # this basically fixes the structure of pd data frame
        self.intent_mappings = pd.read_csv("data/intent_response_maps/intent_response.csv")
        self.intent_mappings.fillna("", inplace=True)
        self.intent_mappings.entities = \
            self.intent_mappings.entities.map(lambda entities: {e.strip() for e in entities.split(",")})

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any],) -> List[EventType]:
        """

        Parameters
        ----------
        dispatcher
        tracker
        domain

        Returns
        -------

        """
        # -- globals --
        _do_not_show_intents = {"out_of_scope", "stop", "contact_human"}

        _sorry_utterances = ["Sorry. I'm not sure I've understood you correctly. "
                             "Can you pick some of the following options",
                             "I apologize, but I did not get that. "
                             "Can you pick one of the following options?"]

        intent_ranking = tracker.latest_message.get("intent_ranking", [])

        # this is the nlu threshold, when I consider the script names valid for `receiving the signal`
        _low_threshold = 0.3

        #print('>>> INTENT RANKING: ', intent_ranking)

        # -- step 0 -- grab only uncertain intents only if their confidence <0.2
        # TODO: I do not like this
        #if len(intent_ranking) > 1:
            # calculate the confidence difference
        #    diff_intent_confidence = intent_ranking[0].get("confidence") - intent_ranking[1].get("confidence")

        #   if diff_intent_confidence < 0.2:
        #        intent_ranking = intent_ranking[:2]
        #    else:
        #        intent_ranking = intent_ranking[:1]

        # -- step 0 -- filter the intents for {"out_of_scope", "stop", "contact_human"}
        intent_ranking = filter(lambda x: x['name'] not in _do_not_show_intents, intent_ranking)

        # -- step 1 -- ok, now filter for very low probabilities
        intent_ranking = list(filter(lambda x: x['confidence'] > _low_threshold, intent_ranking))

        # -- step 2 -- ok, if intent `intent_ranking` does not exist, reformulate
        if not intent_ranking:
            global _COUNT
            global _UPPER_BOUND

            if _COUNT < _UPPER_BOUND:
                _COUNT += 1
                dispatcher.utter_template('utter_rephrase', tracker)

                # TODO: check this ?? return [UserUtteranceReverted()]
                return [UserUtteranceReverted()]
            else:
                _COUNT = 0
                dispatcher.utter_template('utter_start_over', tracker)
                dispatcher.utter_template('utter_goodbye', tracker)
                return [FollowupAction('action_restart')]

        # -- step 3 -- if you are still here, this means, you have some nontrivial intents
        #print('>>> INTENT RANKING 2: ', intent_ranking)

        # -- step  1 -- filter all real intents (you do not want the "out_of_scope")
        _first_intent_names, _first_intent_rankings = zip(*map(lambda x: (x['name'], x['confidence']), intent_ranking))

        # -- step 2 -- you must decide whether you will send just reformulate utterance
        # or the utterance with button
        # now, if in the `first_intent_names` are the names for scripts (i.e. script intents) then send
        # only the reformulate utterance, otherwise send the button utterance.
        _script_set = set(_first_intent_names) & set(_config["script_names"])

        if _script_set:
            if _first_intent_rankings[0] >= _low_threshold:
                _trans = self.intent_mappings.to_dict()

                # you must turn it into the useful dictionary
                _trans = dict(zip(_trans['intent'].values(), _trans['button'].values()))

                dispatcher.utter_message(text='We think, you might mean the following problem:')
                dispatcher.utter_message(text='"{}"'.format('\n'.join(map(lambda x: _trans[x], _script_set))))
                dispatcher.utter_message(text="But, we are unable to resolve the problem description reliably.")
                dispatcher.utter_message(text="Can you describe more specifically?")
            else:
                dispatcher.utter_template('utter_rephrase', tracker)
        else:

            # -- step 2 -- this handles the entities
            entities = tracker.latest_message.get("entities", [])
            entities = {e["entity"]: e["value"] for e in entities}

            # -- step 3 -- dump as a json into memory
            entities_json = json.dumps(entities)

            #print('>>> ENTITIES: ', entities)

            buttons = []

            # -- step -- take only first 2 intents (max)
            for intent in _first_intent_names[:2]:
                # log it
                logger.debug(intent)
                logger.debug(entities)

                # append to buttons
                buttons.append({"title": self.get_button_title(intent, entities),
                                "payload": "/{}{}".format(intent, entities_json), })

            # /out_of_scope is a retrieval intent
            # you cannot send rasa the '/out_of_scope' intent
            # instead, you can send one of the sentences that it will map onto the response
            buttons.append({"title": "Contact human help.",
                            "payload": "/contact_human{}"})

            # also append the button with the continue conversation
            buttons.append({"title": "Continue in conversation.",
                            "payload": "/continue{}"})

            # --- !!! if it is not the script message, then give some buttons else, just field about reformulation!!! ---
            message_title = random.choice(_sorry_utterances)

            dispatcher.utter_message(text=message_title, buttons=buttons)

        # THIS MUST BE THERE: UserUtteranceReverted()
        return [UserUtteranceReverted()]

    def get_button_title(self, intent: Text, entities: Dict[Text, Text]) -> Text:
        """

        Parameters
        ----------
        intent
        entities

        Returns
        -------
        Text
        """
        # -- step 0 -- this gets the bool series
        default_utterance_query = self.intent_mappings.intent == intent

        utterance_query = (self.intent_mappings.entities == entities.keys()) & (default_utterance_query)

        utterances = self.intent_mappings[utterance_query].button.tolist()

        if len(utterances) > 0:
            button_title = utterances[0]
        else:
            utterances = self.intent_mappings[default_utterance_query].button.tolist()
            button_title = utterances[0] if len(utterances) > 0 else intent

        return button_title.format(**entities)


class ActionGoBack(Action):
    """This action s

    """
    def name(self) -> Text:
        return "action_go_back"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> \
        List[EventType]:
        """
        """
        return [UserUtteranceReverted()]

# log actions


class ActionRequestLog(Action):
    """This action requests the local agent to send the logs

    """
    def name(self) -> Text:
        return "action_retrieve_logs"

    def run(self, dispatcher, tracker: Tracker, domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        """

        Parameters
        ----------
        dispatcher
        tracker
        domain

        Returns
        -------

        """
        dispatcher.utter_message(text='<RETRIEVE_LOGS>')

        return []





